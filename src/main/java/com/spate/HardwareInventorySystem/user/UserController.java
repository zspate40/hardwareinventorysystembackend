package com.spate.HardwareInventorySystem.user;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.json.JSONObject;

import com.spate.HardwareInventorySystem.security.AuthenticationRequest;
import com.spate.HardwareInventorySystem.security.JwtTokenProvider;

import io.jsonwebtoken.Claims;

import static java.util.stream.Collectors.toList;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path="/user")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@PostMapping("/add")
	public ResponseEntity addUser(@RequestBody User user) {
		if(userRepository.existsById(user.getUserId())){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("UserId already in use");
		}else {
		userRepository.save(user);
		user.setUserPassword(null);
		return ResponseEntity.ok().body(user);
		}
	}

	@GetMapping("/{userId}")
	public @ResponseBody Optional<User> getUser(@PathVariable Integer userId)  {
	       return userRepository.findById(userId);
	    }
	
	@GetMapping
    public @ResponseBody Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }
	
	@PostMapping("/login")
	public @ResponseBody ResponseEntity login(@RequestBody User data)  {
		    if (userRepository.existsById(data.getUserId())) {
				Optional<User> optionalUser =  userRepository.findById(data.getUserId());
				User serverUser = optionalUser.get();
				HttpHeaders responseHeaders = new HttpHeaders();
				   responseHeaders.set("MyResponseHeader", "MyValue");
				if (serverUser.getPassword().equals(data.getPassword())) {
					String token = jwtTokenProvider.createToken(serverUser.getUserId(), this.userRepository.findById(serverUser.getUserId()).orElseThrow(() -> new UsernameNotFoundException("User Id " + data.getUserId() + "not found")).getAuthorities());

		            Map<Object, Object> model = new HashMap<>();
		            model.put("userId", serverUser.getUserId());
		            model.put("token", token);
		            return ResponseEntity.ok(model);
				}
				else {
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Incorrect password");
				}
		    }
		    else {
		    	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User Not Found");
		    }
		}

		
		/**try {
            int userId = data.getUserId();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userId, data.getPassword()));
            String token = jwtTokenProvider.createToken(userId, this.userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User Id " + userId + "not found")).getAdmin());

            Map<Object, Object> model = new HashMap<>();
            model.put("userId", userId);
            model.put("token", token);
            return ResponseEntity.ok(model);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied");}
	}
	*/
	@DeleteMapping("/{userId}")
    public @ResponseBody String deleteUser(@PathVariable Integer userId) {
		userRepository.deleteById(userId);
        return "Deleted " + userId;
    }

	@PostMapping("/update/{userId}")
	public String updateUser(@RequestBody User user, @PathVariable Integer userId) {
		
		if(userRepository.existsById(userId))
		{
			user.setUserId(userId);
			userRepository.save(user);
			return String.format("Updated %s.", user);
		}
		else
		{
			return "User not found";
		}
	}
	
	@GetMapping("/me")
    public ResponseEntity currentUser(@AuthenticationPrincipal UserDetails userDetails){
        Map<Object, Object> model = new HashMap<>();
        model.put("userId", userDetails.getUsername());
        model.put("roles", userDetails.getAuthorities()
            .stream()
            .map(a -> ((GrantedAuthority) a).getAuthority())
            .collect(toList())
        );
        return ResponseEntity.ok(model);
    }
}
