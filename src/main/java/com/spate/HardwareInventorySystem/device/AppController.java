package com.spate.HardwareInventorySystem.device;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path="/device")
public class AppController {
	@Autowired
	private DeviceRepository deviceRepository;

	@PostMapping("/add")
	public ResponseEntity addDevice(@RequestBody Device device) {		
		deviceRepository.save(device);
		return ResponseEntity.ok(device);
	}

	
	@GetMapping("/{deviceId}")
	public @ResponseBody Optional<Device> getDevice(@PathVariable Integer deviceId)  {
	       return deviceRepository.findById(deviceId);
	    }
	
	@DeleteMapping("/{deviceId}")
    public ResponseEntity deleteDevice(@PathVariable Integer deviceId) {
        deviceRepository.deleteById(deviceId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
    }

	@PostMapping("/update")
	public ResponseEntity updateDevice(@RequestBody Device device) {
		
		if(deviceRepository.existsById(device.getDeviceID()))
		{
			
			deviceRepository.save(device);
			return ResponseEntity.ok(device);
			//return String.format("Updated %s.", device);
		}
		else
		{
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Device not found");
		}
	}

	
}
