package com.spate.HardwareInventorySystem.device;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "devices")
public class Device {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int deviceID;
	
	private int deviceTypeId;
	
	private Boolean checkedOut;
	
	@Column(nullable = true)
	private int userID;
	
	@Column(nullable = true)
	private Date dateCheckedOut;
	
	public int getDeviceID() {
		return deviceID;
	}
	
	public void setID(int id) {
		this.deviceID = id;
	}
	
	public int getDeviceType() {
		return deviceTypeId;
	}
	
	public void setDeviceType(int type) {
		this.deviceTypeId = type;
	}
	
	public Boolean getCheckedOut() {
		return checkedOut;
	}
	
	public void setCheckedOut(Boolean status) {
		this.checkedOut = status;
	}
	
	public int getUserID() {
		return userID;
	}
	
	public void setUserID(int user) {
		this.userID = user;
	}
	
	public Date getCheckedOutDate() {
		return dateCheckedOut;
	}
	
	public void setCheckedOutDate(Date date) {
		this.dateCheckedOut = date;
	}
	
	 @Override
	    public String toString() {
		
		 String theDate = "null";
		 String theUser = "" + userID;
		 if(dateCheckedOut != null)
		 {
			 theDate = dateCheckedOut.toString();
		 }
		 if(!checkedOut)
		 {
			 theUser = "null";
		 }
	 
	        return "Device{" +
	                "Device Id=" + deviceID +
	                ", Device Type Id=" + deviceTypeId + 
	                ", Checked out='" + checkedOut + '\'' +
	                ", Date Checked Out=" + theDate  +
	                ", User Id=" + theUser  +
	                '}';
	
	 }
	

}
