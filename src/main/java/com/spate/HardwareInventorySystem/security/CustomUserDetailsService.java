package com.spate.HardwareInventorySystem.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.spate.HardwareInventorySystem.user.UserRepository;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository users;

    public CustomUserDetailsService(UserRepository users) {
        this.users = users;
    }

    
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        return (UserDetails) this.users.findById(Integer.parseInt(userId))
            .orElseThrow(() -> new UsernameNotFoundException("User Id: " + userId + " not found"));
    }




}