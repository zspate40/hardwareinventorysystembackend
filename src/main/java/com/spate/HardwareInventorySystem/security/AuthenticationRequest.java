package com.spate.HardwareInventorySystem.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest implements Serializable {
    private int userId;
    private String password;
    
    public int getUserId() {
    	return this.userId;
    }
    
    public String getPassword() {
    	return this.password;
    }
}
