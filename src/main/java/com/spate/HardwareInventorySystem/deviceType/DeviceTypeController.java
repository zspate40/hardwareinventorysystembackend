package com.spate.HardwareInventorySystem.deviceType;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path="/deviceType")
public class DeviceTypeController {
	
	@Autowired
	private DeviceTypeRepository deviceTypeRepository;

	@PostMapping("/add")
	public String addDeviceType(@RequestBody DeviceType deviceType) {
		deviceTypeRepository.save(deviceType);
		return String.format("Added %s.", deviceType);
	}

	@GetMapping("/{deviceTypeId}")
	public @ResponseBody Optional<DeviceType> getDevice(@PathVariable Integer deviceTypeId)  {
	       return deviceTypeRepository.findById(deviceTypeId);
	    }
	
	@GetMapping
    public @ResponseBody Iterable<DeviceType> getAllDeviceTypes() {
        return deviceTypeRepository.findAll();
    }
	
	@DeleteMapping("/{deviceTypeId}")
    public @ResponseBody String deleteDevice(@PathVariable Integer deviceTypeId) {
		deviceTypeRepository.deleteById(deviceTypeId);
        return "Deleted " + deviceTypeId;
    }

	@PostMapping("/update/{deviceTypeId}")
	public String updateDevice(@RequestBody DeviceType deviceType, @PathVariable Integer deviceTypeId) {
		
		if(deviceTypeRepository.existsById(deviceTypeId))
		{
			deviceType.setDeviceTypeId(deviceTypeId);
			deviceTypeRepository.save(deviceType);
			return String.format("Updated %s.", deviceType);
		}
		else
		{
			return "Device Type not found";
		}
	}

}
