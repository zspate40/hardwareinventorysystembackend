package com.spate.HardwareInventorySystem.deviceType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "devicetype")
public class DeviceType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="deviceTypeId")
	private int deviceTypeId;
	
	@Column(name="deviceTypeName")
	private String deviceTypeName;
	
	
	public int getDeviceTypeId() {
		return deviceTypeId;
	}
	
	public void setDeviceTypeId(int typeId) {
		this.deviceTypeId = typeId;
	}
	
	public String getDeviceTypeName() {
		return deviceTypeName;
	}
	
	public void setDeviceTypeName(String name) {
		this.deviceTypeName = name;
	}
	
	 @Override
	    public String toString() {
		
	 
	        return "DeviceType{" +
	                ", Device Type Id=" + deviceTypeId + 
	                ", Device Type Name='" + deviceTypeName +
	                '}';
	
	 }

}
