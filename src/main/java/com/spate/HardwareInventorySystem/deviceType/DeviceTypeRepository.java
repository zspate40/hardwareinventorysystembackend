package com.spate.HardwareInventorySystem.deviceType;

import org.springframework.data.repository.CrudRepository;

public interface DeviceTypeRepository extends CrudRepository<DeviceType, Integer> {

}
