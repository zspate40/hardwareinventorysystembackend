package com.spate.HardwareInventorySystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HardwareInventorySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(HardwareInventorySystemApplication.class, args);
	}
}
